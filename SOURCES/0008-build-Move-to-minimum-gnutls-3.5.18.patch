From cd4f3bed33d5ffdba6846d270c0e11713bc1caf6 Mon Sep 17 00:00:00 2001
From: "Richard W.M. Jones" <rjones@redhat.com>
Date: Tue, 25 Jun 2024 10:55:54 +0100
Subject: [PATCH] build: Move to minimum gnutls >= 3.5.18
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

This version matches current qemu.

RHEL 7 gnutls is too old (lacks gnutls_session_set_verify_cert), which
means TLS will be disabled on this platform.  RHEL 8 has gnutls 3.6.14.

I also unconditionally enabled the gnutls/socket.h header.  This
header was added in 2016 (gnutls 3.5.3), so it's not present in RHEL 7.

On RHEL 7 the configure-time test now prints:

  checking for GNUTLS... no
  configure: WARNING: gnutls not found or < 3.5.18, TLS support will be disabled.
  ...
  Optional library features:
      TLS support ............................ no

Reviewed-by: Daniel P. Berrangé <berrange@redhat.com>
(cherry picked from commit 5ff09cdbbd19226dd2d5015d76134f88dee9321e)
(cherry picked from commit cb6df4f81a97d5d58385d89b0135039f1eddee15)
---
 configure.ac | 12 +++---------
 lib/crypto.c |  5 +----
 2 files changed, 4 insertions(+), 13 deletions(-)

diff --git a/configure.ac b/configure.ac
index da3dc38a..29e3b47a 100644
--- a/configure.ac
+++ b/configure.ac
@@ -94,12 +94,13 @@ AC_ARG_WITH([gnutls],
     [],
     [with_gnutls=check])
 AS_IF([test "$with_gnutls" != "no"],[
-    PKG_CHECK_MODULES([GNUTLS], [gnutls >= 3.3.0], [
+    PKG_CHECK_MODULES([GNUTLS], [gnutls >= 3.5.18], [
+        printf "gnutls version is "; $PKG_CONFIG --modversion gnutls
         AC_SUBST([GNUTLS_CFLAGS])
         AC_SUBST([GNUTLS_LIBS])
         AC_DEFINE([HAVE_GNUTLS],[1],[gnutls found at compile time.])
     ], [
-        AC_MSG_WARN([gnutls not found or < 3.3.0, TLS support will be disabled.])
+        AC_MSG_WARN([gnutls not found or < 3.5.18, TLS support will be disabled.])
     ])
 ])
 AM_CONDITIONAL([HAVE_GNUTLS], [test "x$GNUTLS_LIBS" != "x"])
@@ -114,13 +115,6 @@ AS_IF([test "$GNUTLS_LIBS" != ""],[
     AC_MSG_RESULT([$tls_priority])
     AC_DEFINE_UNQUOTED([TLS_PRIORITY],["$tls_priority"],
                        [Default TLS session priority string])
-
-    # Check for APIs which may not be present.
-    old_LIBS="$LIBS"
-    LIBS="$GNUTLS_LIBS $LIBS"
-    AC_CHECK_FUNCS([\
-	gnutls_session_set_verify_cert])
-    LIBS="$old_LIBS"
 ])
 
 dnl certtool (part of GnuTLS) for testing TLS with certificates.
diff --git a/lib/crypto.c b/lib/crypto.c
index a9b3789c..705e114a 100644
--- a/lib/crypto.c
+++ b/lib/crypto.c
@@ -28,6 +28,7 @@
 
 #ifdef HAVE_GNUTLS
 #include <gnutls/gnutls.h>
+#include <gnutls/socket.h>
 #endif
 
 #include "internal.h"
@@ -512,12 +513,8 @@ set_up_certificate_credentials (struct nbd_handle *h,
   return NULL;
 
  found_certificates:
-#ifdef HAVE_GNUTLS_SESSION_SET_VERIFY_CERT
   if (h->hostname && h->tls_verify_peer)
     gnutls_session_set_verify_cert (session, h->hostname, 0);
-#else
-  debug (h, "ignoring nbd_set_tls_verify_peer, this requires GnuTLS >= 3.4.6");
-#endif
 
   err = gnutls_credentials_set (session, GNUTLS_CRD_CERTIFICATE, ret);
   if (err < 0) {
-- 
2.43.0

